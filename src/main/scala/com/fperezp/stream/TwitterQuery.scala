package com.fperezp.stream

import twitter4j.FilterQuery

trait TwitterQuery {

  val tweetsByContentQuery: List[String] => FilterQuery =
    (terms: List[String]) => new FilterQuery().track(terms: _*)
}

object TwitterQuery extends TwitterQuery
