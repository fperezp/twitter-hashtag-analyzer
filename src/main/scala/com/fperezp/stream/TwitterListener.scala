package com.fperezp.stream

import akka.NotUsed
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Keep, Sink, Source}
import akka.stream.{ActorMaterializer, OverflowStrategy}
import com.fperezp.TwitterAnalyzerConfig
import com.fperezp.actor.Model.Hashtags
import twitter4j._
import twitter4j.conf.ConfigurationBuilder

/**
 * TwitterListener
 *
 * Custom Akka Streams connector for twitter4j. It takes advantage of the StatusListener onStatus hook
 * to inject the tweet data we are interested in into Akka Streams.
 *
 * @param config TwitterAnalyzerConfig configuration object containing the app settings.
 */
class TwitterListener(implicit config: TwitterAnalyzerConfig) {

  implicit protected val system       = ActorSystem("Twitter-Listener")
  implicit protected val materializer = ActorMaterializer.create(system)

  protected val log = system.log

  def source(query: FilterQuery): Source[Hashtags, NotUsed] = {

    val (actorRef, publisher) = Source
      .actorRef[Hashtags](1000, OverflowStrategy.fail)
      .toMat(Sink.asPublisher(true))(Keep.both)
      .run()

    val configurationBuilder = new ConfigurationBuilder
    configurationBuilder
      .setDebugEnabled(true)
      .setOAuthConsumerKey(config.credentials.consumerKey)
      .setOAuthConsumerSecret(config.credentials.consumerSecret)
      .setOAuthAccessToken(config.credentials.accessToken)
      .setOAuthAccessTokenSecret(config.credentials.accessTokenSecret)

    val statusListener = new StatusListener {
      override def onTrackLimitationNotice(numberOfLimitedStatuses: Int): Unit = ()

      override def onException(ex: Exception): Unit = ()

      override def onStallWarning(warning: StallWarning): Unit = ()

      override def onDeletionNotice(statusDeletionNotice: StatusDeletionNotice): Unit = ()

      override def onScrubGeo(userId: Long, upToStatusId: Long): Unit = ()

      override def onStatus(status: Status): Unit = {

        log.debug("New tweet received: {}", status.getText)

        val hashtags = status.getHashtagEntities.map(_.getText).toList
        actorRef ! Hashtags(hashtags)
      }
    }

    val twitterStreaming = new TwitterStreamFactory(configurationBuilder.build).getInstance

    twitterStreaming.addListener(statusListener)
    twitterStreaming.filter(query)

    Source.fromPublisher(publisher)
  }
}
