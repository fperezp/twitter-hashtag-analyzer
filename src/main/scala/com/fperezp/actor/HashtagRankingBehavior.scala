package com.fperezp.actor

import akka.actor.ActorSystem
import com.fperezp.actor.Model.{HashtagRankPosition, HashtagRanking}

class HashtagRankingBehavior(implicit system: ActorSystem) {

  private[this] val log = system.log

  def updateHits(hashtag: String, hitsAcc: Map[String, Int]): Map[String, Int] = {
    val currentHits = hitsAcc.getOrElse(hashtag.toLowerCase(), 0)
    log.debug("Updating hits for Hashtag({}). Current hits: {}", hashtag, currentHits)
    hitsAcc + (hashtag.toLowerCase() -> (currentHits + 1))
  }

  def createRanking(hitsAcc: Map[String, Int], rankingSize: Int): HashtagRanking = {

    log.debug("Creating new Ranking of {} elements", rankingSize)

    val topRanking = hitsAcc.toSeq.sortBy(-_._2).slice(0, rankingSize)

    HashtagRanking((topRanking.zipWithIndex map {
      case ((hashtag, hitsCount), position) =>
        hashtag -> HashtagRankPosition(position + 1, hitsCount)
    }).toMap)
  }
}
