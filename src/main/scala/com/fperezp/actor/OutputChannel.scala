package com.fperezp.actor

import java.time.format.DateTimeFormatter

import com.fperezp.actor.Model.{HashtagRankPosition, HashtagRanking}

/**
 * The intention under this interface is that we can define different output channels.
 * e.g.: publish the diff to a file.
 */
abstract class OutputChannel {
  def publishDiff(currRanking: HashtagRanking, newRanking: HashtagRanking): Unit
}

class ConsoleOutputChannel() extends OutputChannel {

  import Console._

  override def publishDiff(currRanking: HashtagRanking, newRanking: HashtagRanking): Unit = {

    printHeader(currRanking, newRanking)

    newRanking.ranking.toSeq.sortBy(_._2.position).foreach { current =>
      val (hashtag, hashtagPosition) = current
      printRow(currRanking, hashtag, hashtagPosition)
    }

    printFooter()

  }

  private[this] def printFooter(): Unit =
    printf("".padTo(89, '_') + "\n\n")

  private[this] def printHeader(prevRanking: HashtagRanking, currRanking: HashtagRanking): Unit = {
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH.mm.ss")

    printf(
      s"Tweets from ${prevRanking.createdAt.format(formatter)} to ${currRanking.createdAt.format(formatter)}\n")
    printf("".padTo(89, '_') + "\n")
    printf("| Position \t| Hashtag \t\t\t| Count \t| Upward/Downward \t|\n")
    printf("".padTo(89, '_') + "\n")
  }

  private[this] def printRow(
      prevRanking: HashtagRanking,
      hashtag: String,
      currentPosition: HashtagRankPosition): Unit = {
    val previousPos: Option[Int] = prevRanking.ranking.get(hashtag) map (_.position)
    val positionDiff             = previousPos map (prevPos => prevPos - currentPosition.position)

    val changeSymbol = positionDiff map { difference =>
      if (difference > 0) s"${Console.GREEN}↑ ${difference.abs}${Console.RESET}"
      else if (difference == 0) "="
      else s"${Console.RED}↓ ${difference.abs}${Console.RESET}"
    } getOrElse s"${Console.BLUE}New${Console.RESET}"

    printf(s"| ${currentPosition.position} \t\t| ${hashtag
      .padTo(25, ' ')} \t| ${currentPosition.totalHits} \t\t| $changeSymbol \t\t\t|\n")
  }
}
