package com.fperezp.actor

import akka.actor.{Actor, ActorLogging, Props, Stash}
import com.fperezp.TwitterAnalyzerConfig
import com.fperezp.actor.Model._

import scala.concurrent.ExecutionContext

/**
 * HashtagRankActor
 * This actor is in charge of counting hashtags, generate a hashtag ranking and publish it.
 * It stores a hitAccumulator which is used to generate a ranking representation.
 *
 * This actor can received 3 different kind of messages: Hashtags, PublishRanking and RankingPublished.
 *
 * - Hashtags(List[String]): This command tracks a list of hashtags, counting how many times it has been
 *   used in Twitter.
 * - PublishRanking: publish a ranking to the output channel. It also changes the behavior to
 *   `publicationInProgress`
 *   while the publication is in progress. This message is fired by an Akka Scheduler (config value:
 *   publishRankingInterval)
 * - RankingPublished: Once a ranking has been published, this message is send to restore the original
 *   behavior.
 *
 * @param behavior: HashtagRankingBehavior - Contains the logic to update hits and create a ranking.
 * @param outputChannel: The target where the ranking is going to be published.
 * @param ec: Execution Context for async operations.
 * @param config: TwitterAnalyzerConfig configuration object containing the app settings.
 */
class HashtagRankActor(behavior: HashtagRankingBehavior, outputChannel: OutputChannel)(
    implicit ec: ExecutionContext,
    config: TwitterAnalyzerConfig
) extends Actor
    with Stash
    with ActorLogging {

  val resetHitsAcc: Map[String, Int] = Map.empty
  var currentRanking: HashtagRanking = HashtagRanking(Map.empty[String, HashtagRankPosition])
  var hitsAcc: Map[String, Int]      = resetHitsAcc

  // Fires a PublishRanking message to this same actor every `publishRankingInterval interval period
  context.system.scheduler.schedule(
    initialDelay = config.publishRankingInterval,
    interval = config.publishRankingInterval,
    receiver = self,
    message = PublishRanking)

  override def receive: Receive = {
    case Hashtags(terms) if terms.nonEmpty =>
      terms foreach { t =>
        hitsAcc = behavior.updateHits(t, hitsAcc)
      }

      log.debug("Hits updated: {}", hitsAcc)
      sender ! HashtagProccessed

    case Hashtags(_) =>
      log.debug("Ignoring empty Hashtag received.")
      sender ! HashtagProccessed

    case PublishRanking =>
      log.debug("Publish Ranking started")
      context.become(publicationInProgress)

      val newRanking = behavior.createRanking(hitsAcc, config.rankingSize)
      log.debug("New ranking generated")

      outputChannel.publishDiff(currentRanking, newRanking)
      log.debug("New ranking diff published")

      currentRanking = newRanking
      log.debug("Ranking updated")

      if (config.resetHits) hitsAcc = resetHitsAcc

      self ! RankingPublished
  }

  def publicationInProgress: Receive = {
    case RankingPublished =>
      log.debug("Publish Ranking finished.")
      context.unbecome()
      log.debug("Replaying stashed messages.")
      unstashAll()
    case anyMsg =>
      log.debug("Stashing new message received while publishing: {}", anyMsg)
      stash()
  }
}

object HashtagRankActor {
  def props(behavior: HashtagRankingBehavior, outputChannel: OutputChannel)(
      implicit ec: ExecutionContext,
      config: TwitterAnalyzerConfig): Props =
    Props(new HashtagRankActor(behavior, outputChannel))
}
