package com.fperezp.actor

import java.time.LocalDateTime

object Model {

  final case class Hashtags(terms: List[String])

  final case class HashtagRankPosition(position: Int, totalHits: Int)

  final case class HashtagRanking(
      ranking: Map[String, HashtagRankPosition],
      createdAt: LocalDateTime = LocalDateTime.now)

  case object HashtagProccessed

  case object PublishRanking

  case object RankingPublished

}
