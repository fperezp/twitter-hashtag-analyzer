package com.fperezp

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.stream.ActorMaterializer
import akka.stream.scaladsl._
import akka.util.Timeout
import com.fperezp.actor.{ConsoleOutputChannel, HashtagRankActor, HashtagRankingBehavior}
import com.fperezp.stream.{TwitterListener, TwitterQuery}
import com.typesafe.config.{Config, ConfigFactory}

import scala.concurrent.Future

object TwitterAnalyzer extends App {

  implicit val system: ActorSystem             = ActorSystem("actor-system")
  implicit val materializer: ActorMaterializer = ActorMaterializer.create(system)

  val config: Config = ConfigFactory.load().getConfig("twitter-analyzer")

  implicit val applicationConfig: TwitterAnalyzerConfig = TwitterAnalyzerConfig(config)
  implicit val timeout: Timeout                         = Timeout(applicationConfig.timeout)

  import system.dispatcher

  val behavior      = new HashtagRankingBehavior()
  val outputChannel = new ConsoleOutputChannel()

  val hashtagHandler = system.actorOf(HashtagRankActor.props(behavior, outputChannel))

  new TwitterListener()
    .source(TwitterQuery.tweetsByContentQuery(applicationConfig.searchTerms))
    .mapAsync(applicationConfig.parallelism) { hashtags =>
      hashtagHandler ? hashtags recover {
        case err =>
          system.log.error("Hashtags({}) didn't finished with error: {}", hashtags, err.getMessage)
          Future.failed(err)
      }
    }
    .runWith(Sink.ignore)
    .onComplete(_ => system.terminate())
}
