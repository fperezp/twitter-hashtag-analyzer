package com.fperezp

import java.util.concurrent.TimeUnit

import com.typesafe.config.Config

import scala.collection.JavaConverters._
import scala.concurrent.duration.FiniteDuration

final case class TwitterAnalyzerConfig(
    publishRankingInterval: FiniteDuration,
    resetHits: Boolean,
    rankingSize: Int,
    searchTerms: List[String],
    credentials: TwitterCredentials,
    timeout: FiniteDuration,
    parallelism: Int
)

final case class TwitterCredentials(
    consumerKey: String,
    consumerSecret: String,
    accessToken: String,
    accessTokenSecret: String
)

object TwitterCredentials {
  def apply(config: Config): TwitterCredentials =
    new TwitterCredentials(
      consumerKey = config.getString("consumerKey"),
      consumerSecret = config.getString("consumerSecret"),
      accessToken = config.getString("accessToken"),
      accessTokenSecret = config.getString("accessTokenSecret")
    )
}

object TwitterAnalyzerConfig {

  def apply(config: Config): TwitterAnalyzerConfig =
    new TwitterAnalyzerConfig(
      publishRankingInterval =
        FiniteDuration(config.getDuration("publishRankingInterval").getSeconds, TimeUnit.SECONDS),
      resetHits = config.getBoolean("resetHits"),
      rankingSize = config.getInt("rankingSize"),
      searchTerms = config.getStringList("terms").asScala.toList,
      credentials = TwitterCredentials(config.getConfig("credentials")),
      timeout = FiniteDuration(config.getDuration("timeout").getSeconds, TimeUnit.SECONDS),
      parallelism = config.getInt("parallelism")
    )
}
