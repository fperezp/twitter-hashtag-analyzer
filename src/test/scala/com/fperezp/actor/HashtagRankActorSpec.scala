package com.fperezp.actor

import akka.actor.{ActorRef, ActorSystem}
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import com.fperezp.TwitterAnalyzerConfig
import com.fperezp.actor.Model.{HashtagProccessed, Hashtags, PublishRanking}
import com.typesafe.config.ConfigFactory
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{FlatSpecLike, Matchers}

class HashtagRankActorSpec
    extends TestKit(ActorSystem("TestingActorSystem"))
    with FlatSpecLike
    with Matchers
    with ImplicitSender
    with MockitoSugar {

  import system.dispatcher

  implicit val config = TwitterAnalyzerConfig(ConfigFactory.load().getConfig("twitter-analyzer"))

  val hashtagRankingbehavior = new HashtagRankingBehavior
  val outputChannel          = mock[OutputChannel]

  val hashtagRankActor: ActorRef =
    system.actorOf(HashtagRankActor.props(hashtagRankingbehavior, outputChannel))

  "HashtagRankActor" should "Sending valid fullfilled Hashtags message fires a HashtagProcessed one" in {

    hashtagRankActor ! Hashtags(config.searchTerms)
    expectMsg(HashtagProccessed)

  }

  it should "Sending empty Hashtags message fires a HashtagProcessed one" in {

    hashtagRankActor ! Hashtags(List())
    expectMsg(HashtagProccessed)

  }

  it should "Sending unknown message produces no fired message" in {

    hashtagRankActor ! "AnyMsg"
    expectNoMessage()

  }

  it should "Sending PublishRanking and Hashtags fires a HashtagProccessed message after publishing" in {

    hashtagRankActor ! PublishRanking
    hashtagRankActor ! Hashtags(config.searchTerms)
    expectMsg(HashtagProccessed)

  }

  it should "updates accHits when receiving a valid Hashtags message with a sigle term" in {

    val testActorRef: TestActorRef[HashtagRankActor] =
      TestActorRef(HashtagRankActor.props(hashtagRankingbehavior, outputChannel))

    testActorRef ! Hashtags(List("test"))

    testActorRef.underlyingActor.hitsAcc.size shouldBe 1
    testActorRef.underlyingActor.hitsAcc.keys.toList should contain("test")

  }

  it should "updates accHits when receiving a valid Hashtags message with multiple terms" in {

    val testActorRef: TestActorRef[HashtagRankActor] =
      TestActorRef(HashtagRankActor.props(hashtagRankingbehavior, outputChannel))

    testActorRef ! Hashtags(config.searchTerms)

    testActorRef.underlyingActor.hitsAcc.size shouldBe config.searchTerms.size

    config.searchTerms map { oneTerm =>
      testActorRef.underlyingActor.hitsAcc.keys.toList should contain(oneTerm)
    }
  }

  it should "store accHits when receiving a valid PublishRanking message" in {

    val appConfig = config.copy(resetHits = true)

    val testActorRef: TestActorRef[HashtagRankActor] =
      TestActorRef(
        HashtagRankActor.props(hashtagRankingbehavior, outputChannel)(system.dispatcher, appConfig))

    testActorRef ! Hashtags(config.searchTerms)

    val currentAcc = testActorRef.underlyingActor.hitsAcc

    testActorRef ! PublishRanking

    testActorRef.underlyingActor.currentRanking.ranking.keys shouldBe currentAcc.keys
    testActorRef.underlyingActor.currentRanking.ranking.values.toList map (_.totalHits) shouldBe currentAcc.values.toList

  }

  it should "reset accHits when receiving a valid PublishRanking message if resetHits is set to true" in {

    val appConfig = config.copy(resetHits = true)

    val testActorRef: TestActorRef[HashtagRankActor] =
      TestActorRef(
        HashtagRankActor.props(hashtagRankingbehavior, outputChannel)(system.dispatcher, appConfig))

    testActorRef ! Hashtags(config.searchTerms)
    testActorRef ! PublishRanking

    testActorRef.underlyingActor.hitsAcc shouldBe Map.empty[String, Int]
  }

  it should "keep accHits content when receiving a valid PublishRanking message if resetHits is set to false" in {

    val appConfig = config.copy(resetHits = false)
    val testActorRef: TestActorRef[HashtagRankActor] =
      TestActorRef(
        HashtagRankActor.props(hashtagRankingbehavior, outputChannel)(system.dispatcher, appConfig))

    testActorRef ! Hashtags(config.searchTerms)

    val currentAcc = testActorRef.underlyingActor.hitsAcc

    testActorRef ! PublishRanking

    testActorRef.underlyingActor.hitsAcc shouldBe currentAcc
  }
}
