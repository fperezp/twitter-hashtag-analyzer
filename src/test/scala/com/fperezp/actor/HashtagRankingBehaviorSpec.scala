package com.fperezp.actor

import akka.actor.ActorSystem
import akka.testkit.TestKit
import org.scalatest.{FlatSpecLike, Matchers}

class HashtagRankingBehaviorSpec
    extends TestKit(ActorSystem("TestingActorSystem"))
    with FlatSpecLike
    with Matchers {

  val dummyHashtag1 = "hashtag1"
  val dummyHashtag2 = "hashtag2"
  val dummyHashtag3 = "hashtag3"
  val dummyHashtag4 = "hashtag4"
  val dummyHashtag5 = "hashtag5"

  val hashtagBehavior = new HashtagRankingBehavior()

  "updateHits" should "Add a new element to the map for a new Hashtag" in {
    val emptyAcc    = Map.empty[String, Int]
    val updatedHits = hashtagBehavior.updateHits(dummyHashtag1, emptyAcc)

    updatedHits.size shouldBe emptyAcc.size + 1
    updatedHits.get(dummyHashtag1) shouldBe Some(1)
  }

  it should "Increment hits for an existing Hashtag" in {
    val currentHits = 5
    val currentAcc  = Map(dummyHashtag1 -> currentHits)
    val updatedHits = hashtagBehavior.updateHits(dummyHashtag1, currentAcc)

    updatedHits.size shouldBe currentAcc.size
    updatedHits.get(dummyHashtag1) shouldBe Some(currentHits + 1)
  }

  it should "Acc is updated with a new hashtag when acc already contains multiple hashtags" in {

    val currentAcc =
      Map(dummyHashtag1 -> 1, dummyHashtag2 -> 1, dummyHashtag3 -> 2, dummyHashtag4 -> 6)

    val updatedHits = hashtagBehavior.updateHits(dummyHashtag5, currentAcc)

    updatedHits.size shouldBe currentAcc.size + 1
    updatedHits.get(dummyHashtag5) shouldBe Some(1)
  }

  it should "Acc is updated with a right number of hits when updating an already existent hashtag" in {

    val currentAcc =
      Map(
        dummyHashtag1 -> 1,
        dummyHashtag2 -> 1,
        dummyHashtag3 -> 2,
        dummyHashtag4 -> 6,
        dummyHashtag5 -> 3)

    val previousHits = currentAcc.get(dummyHashtag4)
    val updatedHits  = hashtagBehavior.updateHits(dummyHashtag4, currentAcc)

    updatedHits.size shouldBe currentAcc.size
    updatedHits.get(dummyHashtag4) shouldBe previousHits.map(_ + 1)
  }

  "createRanking" should "Creates a ranking from an empty acc" in {
    val emptyAcc = Map.empty[String, Int]
    val ranking  = hashtagBehavior.createRanking(emptyAcc, 10)

    ranking.ranking.size shouldBe emptyAcc.size
  }

  it should "Creates a ranking of n positions from a non empty acc" in {
    val currentAcc =
      Map(
        dummyHashtag1 -> 1,
        dummyHashtag2 -> 1,
        dummyHashtag3 -> 2,
        dummyHashtag4 -> 6,
        dummyHashtag5 -> 3)

    val ranking = hashtagBehavior.createRanking(currentAcc, 2)

    ranking.ranking.size shouldBe 2
  }

  it should "create a ranking positioned by numHits" in {
    val currentAcc =
      Map(
        dummyHashtag1 -> 3,
        dummyHashtag2 -> 2,
        dummyHashtag3 -> 1,
        dummyHashtag4 -> 4,
        dummyHashtag5 -> 5)

    val ranking = hashtagBehavior.createRanking(currentAcc, 3)

    ranking.ranking.size shouldBe 3
    ranking.ranking(dummyHashtag4).position shouldBe 2
    ranking.ranking(dummyHashtag5).position shouldBe 1
    ranking.ranking(dummyHashtag1).position shouldBe 3
  }

}
