import sbt.Keys._
import sbt._

import org.scalafmt.sbt.ScalafmtPlugin.autoImport._

object ProjectPlugin extends AutoPlugin {

  override def trigger: PluginTrigger = allRequirements

  object autoImport {

    lazy val Versions = new {
      val akka          = "2.5.11"
      val scalaTest     = "3.0.5"
      val scalaCheck    = "1.13.4"
      val twitter4j     = "4.0.6"
      val mockito       = "2.7.22"
    }

    lazy val noPublishSettings: Seq[Def.Setting[_]] = Seq(
      publish := ((): Unit),
      publishLocal := ((): Unit),
      publishArtifact := false
    )

    lazy val streamDependencies: Seq[Def.Setting[_]] = Seq(
      libraryDependencies ++= Seq(
        "org.twitter4j"     %  "twitter4j-core"    % Versions.twitter4j,
        "org.twitter4j"     %  "twitter4j-stream"  % Versions.twitter4j,
        "com.typesafe.akka" %% "akka-stream"       % Versions.akka
      )
    )
  }

  import autoImport._

  override def projectSettings: Seq[Def.Setting[_]] =
    Seq(
      name := "twitter-akka",
      organization := "com.spotahome",
      organizationName := "Twitter Akka",
      scalaVersion := "2.12.4",
      resolvers ++= Seq(
        Resolver.sonatypeRepo("snapshots"),
        Resolver.sonatypeRepo("releases")
      ),
      scalacOptions := Seq(
        "-deprecation",
        "-encoding",
        "UTF-8",
        "-feature",
        "-language:existentials",
        "-language:higherKinds",
        "-language:implicitConversions",
        "-unchecked",
        "-Xlint",
        "-Yno-adapted-args",
        "-Ywarn-dead-code",
        "-Ywarn-numeric-widen",
        "-Ywarn-value-discard",
        "-Xfuture",
        "-Ywarn-unused-import",
        "-Ypartial-unification"
      ),
      publishMavenStyle := true,
      publishArtifact in (Compile, packageDoc) := false,
      publishArtifact in packageDoc := false,
      sources in (Compile, doc) := Seq.empty,
      scalafmtCheck := true,
      scalafmtOnCompile := true,
      libraryDependencies ++= Seq(
        "org.scalatest"     %% "scalatest"    % Versions.scalaTest  % Test,
        "org.scalacheck"    %% "scalacheck"   % Versions.scalaCheck % Test,
        "com.typesafe.akka" %% "akka-testkit" % Versions.akka       % Test,
        "org.mockito"       %  "mockito-core" % Versions.mockito    % Test
      )
    )

}
