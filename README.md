## Twitter Hashtag Analyzer

Twitter Hashtag Analyzer reads a continuous stream of tweets containing any of a set of terms, gets
the hashtags for those tweets, and generates a ranking based on the number of times a hashtag has
been mentioned.

### Configuration

Credentials for Twitter must be defined via ENV_VARS or VM_OPTS.

#### Using ENV_VARs

```
$ export TWITTER_CONSUMER_KEY=XXXXXX
$ export TWITTER_CONSUMER_SECRET=XXXXXX
$ export TWITTER_ACCESS_TOKEN=XXXXXX
$ export TWITTER_ACCESS_SECRET=XXXXXX

$ sbt run
```

#### Using VM_OPTS

```
$ sbt 
    -Dtwitter-analyzer.credentials.consumerKey=XXXXXX \  
    -Dtwitter-analyzer.credentials.consumerSecret=XXXXXX \  
    -Dtwitter-analyzer.credentials.accessToken=XXXXXX \
    -Dtwitter-analyzer.credentials.accessTokenSecret=XXXXXX \ 
    ...
```

#### Default configuration:
```
akka {
  loglevel = "INFO"
}

twitter-analyzer {
  publishRankingInterval = 10 seconds
  resetHits         = true
  rankingSize       = 10
  terms             = ["star wars", "justin bieber", "real madrid"]
  
  timeout = 5 seconds
  parallelism = 3
}
```

Some of these values can be overriden as well via ENV_VARS or VM_OPTS.

Table of config fields - ENV_VARS:

| config                    | ENV_VAR                               | Description                                   |
|---------------------------|---------------------------------------|-----------------------------------------------|
| publishRankingInterval    | TWITTER_ANALYZER_SCHEDULER_INTERVAL   | Time interval to publish a new ranking        |
| resetHits                 | TWITTER_ANALYZER_RESET_HITS           | If set to true, hashtag hits accumulator will be be reset. If not, hits won't be deleted and ranking will be generated taking history hits into account|
| rankingSize               | TWITTER_ANALYZER_RANKING_SIZE         | Number of positions the ranking will have     |

In addition, `terms` can be also overriden, but due to some sort of limitations on exporting array vars into bash, it 
only be done through VM_OPTS.

```
$ sbt
    -Dtwitter-analyzer.terms.0="rajoy" 
    -Dtwitter-analyzer.terms.1="barcelona" 
    -Dtwitter-analyzer.terms.2="star trek" 
    ...
```

#### Unit Tests
There is a small set of unit tests available focused on the Actor mechanism and behavior.

Run `sbt clean test` to check them.
Code coverage can be also reviewed running:
```
$ sbt clean coverage test coverageReport
```
This command will generate an HTML report.

_Note: Current code coverage is 41.06 %_

### Evaluation
To test the application through a fatJar:

```
$ sbt clean assembly // Generates a fatJar
$ export TWITTER_CONSUMER_KEY=XXXXXX
$ export TWITTER_CONSUMER_SECRET=XXXXXX
$ export TWITTER_ACCESS_TOKEN=XXXXXX
$ export TWITTER_ACCESS_SECRET=XXXXXX
$ java -jar target/scala-2.12/twitter-akka-root-assembly-0.1.0-SNAPSHOT.jar
``` 

To full customize the execution:
```
$ export TWITTER_CONSUMER_KEY=XXXXXX
$ export TWITTER_CONSUMER_SECRET=XXXXXX
$ export TWITTER_ACCESS_TOKEN=XXXXXX
$ export TWITTER_ACCESS_SECRET=XXXXXX
$ export TWITTER_ANALYZER_SCHEDULER_INTERVAL="5 seconds" 
$ export TWITTER_ANALYZER_RESET_HITS=false
$ export TWITTER_ANALYZER_RANKING_SIZE=15
$ java
    -Dtwitter-analyzer.terms.0="rajoy" 
    -Dtwitter-analyzer.terms.1="barcelona" 
    -Dtwitter-analyzer.terms.2="star trek" 
    -jar target/scala-2.12/twitter-akka-root-assembly-0.1.0-SNAPSHOT.jar
```
