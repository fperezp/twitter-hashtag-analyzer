
lazy val root = project
  .in(file("."))
  .settings(name := "twitter-akka-root")
  .settings(noPublishSettings)
  .settings(mainClass in assembly := Some("com.fperezp.TwitterAnalyzer"))
  .settings(streamDependencies)
